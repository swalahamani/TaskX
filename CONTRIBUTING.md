# TaskX - Contribution Guide:

Project is organised in a way that can include all the *fron-end*, *back-end* codes and other-resources*  in this single repository.


## TaskX : Repository Folder structure and Usage Guide:

>**./db:**
>all data-base related files should be kept in this directory.
>
>**./mobile-app:**
mobile-app code should be kept inside this directory.
>
>**./resources:**
all general resources including andy discussion notes/images/reference>will be kept inside this
>
>**./server:**
server side code will be kept inside this directory.
>
>**./web-app:**
all web app code and related files will be kept inside this.

## Branch Organization
    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project repo is setup in a way to avoid confilicts and making collaborated working more ease.

**[x] Standard Branches**
>
>- **master:**
>stable production ready version of code:
>Public stable releases should be made from this.
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
>- **beta-master:**
>public-beta ready version of code:
>Public beta releases will be out from this code base.
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
>- **alpha-master:**
>closed-apha ready version of code:
>No public-releases are permitted from this code base.
>All releases made from this branch should be tagged/labelled *'alpha-build'*.
>All releases made from this branch should only be used
>for internal testing purposes.
>
**[x] Developer Branches**
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All developers can create branches as they need if the following conditions are met:

>- All developers should create a tleast one branch to push their code. It should be named as
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*dev/{user_name or nick_name}*
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eg: *dev/swalah*
>If you are chose to use nick name, its your duty to ensure your nick name is unique within the organization.
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
>- If you think its better to create a and work on a separated branch to solve a critical
>bug/implement a feature request, you are allowed to create a branch. It should be in the below form:
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*dev/{user_name or nick_name}/{feature or issue branch name}*
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eg: *dev/swalah/animation* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(*a branch for implementing transition animations*).

## Coding Standards:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;At XiStart, all developers are requested to follow the coding standards very strictly to make collaborative working easier than ever! All developers should consider the following while writing codes.

**[x] Coding/Naming Conventions**
>   - **Java** 
>Read from this [link](https://www.geeksforgeeks.org/java-naming-conventions/).
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
>   - **JavaScript**
>Read from [this](https://www.w3schools.com/js/js_conventions.asp) and [this](http://www.j-io.org/Javascript-Naming_Conventions).
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
>   - **ReactNative**
>Read from this [link](https://unbug.gitbooks.io/react-native-training/content/45_naming_convention.html).